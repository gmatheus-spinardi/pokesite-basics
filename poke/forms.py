from django import forms

from django.forms import ModelForm
from .models import Poke, Coment


class PokeForm(ModelForm):
    class Meta:
        model = Poke
        fields = [
            'name',
            
            'poster_url',
            'content',
        ]
        labels = {
            'name': 'Título',
            
            'poster_url': 'URL do Poster',
            'content': 'Notícia',
        }

class ComentForm(ModelForm):
    class Meta:
        model = Coment
        fields = [
            'author',
            'text',
            
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Comentário',
        }