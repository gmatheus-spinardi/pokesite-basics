poke_data = [{
    "id":
    "1",
    "name":
    "The Shawshank Redemption",
    "date":
    "1994",
    "poster_url":
    "https://image.tmdb.org/t/p/w500/q6y0Go1tsGEsmtFryDOJo3dEmqu.jpg"
}, {
    "id":
    "2",
    "name":
    "The Godfather",
    "date":
    "1972",
    "poster_url":
    "https://image.tmdb.org/t/p/w500/3bhkrj58Vtu7enYsRolD1fZdja1.jpg"
}, {
    "id":
    "3",
    "name":
    "Schindler's List",
    "date":
    "1993",
    "poster_url":
    "https://image.tmdb.org/t/p/w500/sF1U4EUQS8YHUYjNl3pMGNIQyr0.jpg"
}]
