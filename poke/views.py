from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from .temp_data import poke_data
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from .models import Poke, Coment, Category
from .forms import PokeForm, ComentForm
from django.shortcuts import render, get_object_or_404
from django.views import generic


def detail_poke(request, poke_id):
    poke = get_object_or_404(Poke, pk=poke_id)
    context = {'poke': poke}
    return render(request, 'poke/detail.html', context)

def list_poke(request):
    poke_list = Poke.objects.all()

    context = {'poke_list': poke_list}
    return render(request, 'poke/index.html', context)

def search_poke(request):
    context = {}
    if request.GET.get('query', False):
        poke_list=[]
        search_term = request.GET['query'].lower()
        poke1_list = Poke.objects.filter(name__icontains=search_term)
        for i in range(len(poke1_list),0,-1):
            i-=1
            poke_list.append(poke1_list[i])
        context = {"poke_list": poke_list}
    return render(request, 'poke/search.html', context)

def create_poke(request):
    if request.method == 'POST':
        poke_name = request.POST['name']
        poke_poster_url = request.POST['poster_url']
        poke_content = request.POST['content']
        poke = Poke(name=poke_name,
                      poster_url=poke_poster_url,
                      content = poke_content )
        poke.save()
        return HttpResponseRedirect(
            reverse('poke:detail', args=(poke.id, )))
    else:
        form = PokeForm()
        context = {'form': form}
        return render(request, 'poke/create.html', context)

def update_poke(request, poke_id):
    poke = get_object_or_404(Poke, pk=poke_id)

    if request.method == "POST":
        poke.name = request.POST['name']
        poke_poster_url = request.POST['poster_url']
        poke.content = request.POST['content']
        poke.save()
        return HttpResponseRedirect(
            reverse('poke:detail', args=(poke.id, )))

    context = {'poke': poke}
    return render(request, 'poke/update.html', context)

def delete_poke(request, poke_id):
    poke = get_object_or_404(Poke, pk=poke_id)

    if request.method == "POST":
        poke.delete()
        return HttpResponseRedirect(reverse('poke:index'))

    context = {'poke': poke}
    return render(request, 'poke/delete.html', context)

def create_coment(request, poke_id):
    poke = get_object_or_404(Poke, pk=poke_id)
    if request.method == 'POST':
        form = ComentForm(request.POST)
        if form.is_valid():
            coment_author = form.cleaned_data['author']
            coment_text = form.cleaned_data['text']
            coment = Coment(author=coment_author,
                            text=coment_text,
                            poke=poke)
            
            coment.save()
            
            return HttpResponseRedirect(
                reverse('poke:detail', args=(poke_id, )))
    else:
        form = ComentForm()
    context = {'form': form, 'poke': poke}
    return render(request, 'poke/coment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'poke/categorys.html'


def detail_category(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    context = {'category': category}
    return render(request, 'poke/category.html', context)


def list_coment(request,poke_id):
    coment1_list = Coment.objects.filter(pk=poke_id)
    coment_list=[]
    for i in range(len(coment1_list),0,-1):
            i-=1
            coment_list.append(coment1_list[i])
    context = {'coment_list': coment_list}
    return render(request, 'poke/detail.html', context)


