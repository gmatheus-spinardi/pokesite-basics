from django.contrib import admin

from .models import Poke, Coment, Category

admin.site.register(Poke)
admin.site.register(Coment)
admin.site.register(Category)