

# Create your models here.
from django.db import models
from django.conf import settings
from datetime import datetime

class Poke(models.Model):
    name = models.CharField(max_length=255)
    date = models. DateTimeField(auto_now_add=True)
    poster_url = models.URLField(max_length=200, null=True)
    content = models.TextField()

    def __str__(self):
        date_time = self.date.strftime("%m/%d/%Y")
        return f'{self.name} ({date_time})'


class Coment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    date = models. DateTimeField(auto_now_add=True)
    poke = models.ForeignKey(Poke, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Category(models.Model):
    
    name = models.CharField(max_length=255)
    text = models.CharField(max_length=255)
    poke = models.ManyToManyField(Poke)

    def __str__(self):
        return f'{self.name} descrição: {self.text}'