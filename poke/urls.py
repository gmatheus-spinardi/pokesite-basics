from django.urls import path

from . import views

app_name = 'poke'
urlpatterns = [
    path('', views.list_poke, name='index'), # adicione esta linha
    path('search/', views.search_poke, name='search'), # adicione esta linha
    path('create/', views.create_poke, name='create'), # adicione esta linha
    path('<int:poke_id>/', views.detail_poke , name='detail'), # adicione esta linha
    path('update/<int:poke_id>/', views.update_poke, name='update'),
    path('delete/<int:poke_id>/', views.delete_poke, name='delete'),
    path('<int:poke_id>/coment/', views.create_coment, name='coment'),
    path('categorys/', views.CategoryListView.as_view(), name='categorys'),
    path('category/<int:category_id>/', views.detail_category, name='category')

]